import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore} from '@angular/fire/firestore';
import { Router} from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import 'firebase/auth';
import { ThrowStmt } from '@angular/compiler';
import  { authModel } from '../model/model';

// import { authModel } from '../../models/model';+

declare var window: any;

 
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  username: string = ""
  password: string = ""
  cpassword: string= ""
  email: string= ""
  number: string= ""

  private eventAuthError = new BehaviorSubject<string>("");
  eventAuthError$ = this.eventAuthError.asObservable();

  newUser: any;

  constructor(private afAuth: AngularFireAuth, private db: AngularFirestore, private router: Router, ) { }


  getUserState(){
    return this.afAuth.authState;
  }

  // public checkAuthState = (callback: any) =>
  // {
  //   this.afAuth.auth().onAuthStateChanged(
  //     (user) => {
  //       callback(user);
  //     }, 
  //     (error) => {
  //       callback(error);
  //     }
  //   );
  // }

  login(email:string, password:string){
    this.afAuth.auth.signInWithEmailAndPassword(email,password)
    .catch(error =>{
      this.eventAuthError.next(error);
    })
    .then(userCredential => {
      if(userCredential){
        this.router.navigateByUrl('/tabs/tab1');
      }
    })
  }

  createUser(user){
    const {username, email, password, cpassword, number } = this
     if (password !== cpassword){
       return  console.error("password don't match")
     }
    this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
    .then( userCredential =>{
      this.newUser = user;
        console.log(userCredential);
      userCredential.user.updateProfile({
        displayName: user.username +''+user.number
      });
      
      if(this.insertUserData){
          this.router.navigateByUrl('/login');
      }  
    }) 

    .catch (error => {
      this.eventAuthError.next(error);
    })
  }

  insertUserData(userCredential: firebase.auth.UserCredential){
    return this.db.doc('users/${userCredential.user.uid}').set({
      email: this.newUser.email,
      number: this.newUser.number,
      username: this.newUser.username, 
      role: 'network user'
    })
    
  }

  logout(){
    return this.afAuth.auth.signOut();
  }
}
