import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database'

@Injectable({ 
  providedIn: 'root'
})
export class FirebasedbService {
   toDoList: AngularFireList<any>;
  constructor(private firebasedb: AngularFireDatabase) { }

  getToDoList(){ 
    this.toDoList = this.firebasedb.list('titles');
    return this.toDoList;
  }
  // in order to add  a todolist item inside the firebasedb, here is the function
  addTitle(title: string){
    this.toDoList.push({
      title: title,
      isChecked: false
    });
  }

  checkOrUnCheckTitle($key: string, flag: boolean){
    this.toDoList.update($key, { ischecked: flag });
  }

  

  removeTitle($key: string){
    this.toDoList.remove($key);
  }

  
}
