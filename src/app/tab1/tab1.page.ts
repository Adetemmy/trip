import { Component } from '@angular/core';
import {  Router } from '@angular/router';
import { AuthService } from 'src/app/providers/auth.service';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  user: firebase.User;
  public logoRetina="assets/img/travel.PNG";
  constructor( private auth: AuthService,  public Router: Router) {}
   ngOnInit(){
     this.auth.getUserState() 
      .subscribe(user => {
        this.user =user;
      })
     
   }
}
