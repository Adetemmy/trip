import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // { path: '', redirecTo: 'register', pathMatch:'full'},
   
  {
    path: '',
    redirectTo: 'register',
    pathMatch: 'full'
  },
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },

  { path: 'auth', loadChildren: './allApp/auth/auth.module#AuthPageModule' },
  // { path: 'home', loadChildren: './allApp/home/home.module#HomePageModule' },
  { path: 'login', loadChildren: './allApp/login/login.module#LoginPageModule' },
  { path: 'fun', loadChildren: './allApp/fun/fun.module#FunPageModule' },
  { path: 'register', loadChildren: './allApp/register/register.module#RegisterPageModule'},
  { path: 'example', loadChildren: './allApp/example/example.module#ExamplePageModule' },




];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
